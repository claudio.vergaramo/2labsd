package escribir

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

type Server struct {
}

func (s *Server) EscribirData(src EscribirService_EscribirDataServer) error {

	for {
		rr, err := src.Recv()

		if err == io.EOF {
			break
		}
		_, err2 := os.Create(rr.Body)

		if err2 != nil {
			fmt.Println(err2)
			os.Exit(1)
		}

		//Escribir en el nodo
		ioutil.WriteFile(rr.Body, rr.ChunkData, os.ModeAppend)

		log.Printf("Chunk guardado")
	}

	return nil
}
