// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0-devel
// 	protoc        v3.13.0
// source: escribir.proto

package escribir

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Data struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Body           string `protobuf:"bytes,1,opt,name=Body,proto3" json:"Body,omitempty"`
	ChunkData      []byte `protobuf:"bytes,2,opt,name=ChunkData,proto3" json:"ChunkData,omitempty"`
	Tipo           int32  `protobuf:"varint,3,opt,name=Tipo,proto3" json:"Tipo,omitempty"` //Tipo de algoritmo (centralizado o distribuido)
	NombreLibro    string `protobuf:"bytes,4,opt,name=NombreLibro,proto3" json:"NombreLibro,omitempty"`
	CantidadChunks uint64 `protobuf:"varint,5,opt,name=CantidadChunks,proto3" json:"CantidadChunks,omitempty"`
}

func (x *Data) Reset() {
	*x = Data{}
	if protoimpl.UnsafeEnabled {
		mi := &file_escribir_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Data) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Data) ProtoMessage() {}

func (x *Data) ProtoReflect() protoreflect.Message {
	mi := &file_escribir_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Data.ProtoReflect.Descriptor instead.
func (*Data) Descriptor() ([]byte, []int) {
	return file_escribir_proto_rawDescGZIP(), []int{0}
}

func (x *Data) GetBody() string {
	if x != nil {
		return x.Body
	}
	return ""
}

func (x *Data) GetChunkData() []byte {
	if x != nil {
		return x.ChunkData
	}
	return nil
}

func (x *Data) GetTipo() int32 {
	if x != nil {
		return x.Tipo
	}
	return 0
}

func (x *Data) GetNombreLibro() string {
	if x != nil {
		return x.NombreLibro
	}
	return ""
}

func (x *Data) GetCantidadChunks() uint64 {
	if x != nil {
		return x.CantidadChunks
	}
	return 0
}

var File_escribir_proto protoreflect.FileDescriptor

var file_escribir_proto_rawDesc = []byte{
	0x0a, 0x0e, 0x65, 0x73, 0x63, 0x72, 0x69, 0x62, 0x69, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x08, 0x65, 0x73, 0x63, 0x72, 0x69, 0x62, 0x69, 0x72, 0x22, 0x96, 0x01, 0x0a, 0x04, 0x44,
	0x61, 0x74, 0x61, 0x12, 0x12, 0x0a, 0x04, 0x42, 0x6f, 0x64, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x04, 0x42, 0x6f, 0x64, 0x79, 0x12, 0x1c, 0x0a, 0x09, 0x43, 0x68, 0x75, 0x6e, 0x6b,
	0x44, 0x61, 0x74, 0x61, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x09, 0x43, 0x68, 0x75, 0x6e,
	0x6b, 0x44, 0x61, 0x74, 0x61, 0x12, 0x12, 0x0a, 0x04, 0x54, 0x69, 0x70, 0x6f, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x04, 0x54, 0x69, 0x70, 0x6f, 0x12, 0x20, 0x0a, 0x0b, 0x4e, 0x6f, 0x6d,
	0x62, 0x72, 0x65, 0x4c, 0x69, 0x62, 0x72, 0x6f, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b,
	0x4e, 0x6f, 0x6d, 0x62, 0x72, 0x65, 0x4c, 0x69, 0x62, 0x72, 0x6f, 0x12, 0x26, 0x0a, 0x0e, 0x43,
	0x61, 0x6e, 0x74, 0x69, 0x64, 0x61, 0x64, 0x43, 0x68, 0x75, 0x6e, 0x6b, 0x73, 0x18, 0x05, 0x20,
	0x01, 0x28, 0x04, 0x52, 0x0e, 0x43, 0x61, 0x6e, 0x74, 0x69, 0x64, 0x61, 0x64, 0x43, 0x68, 0x75,
	0x6e, 0x6b, 0x73, 0x32, 0x45, 0x0a, 0x0f, 0x45, 0x73, 0x63, 0x72, 0x69, 0x62, 0x69, 0x72, 0x53,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x32, 0x0a, 0x0c, 0x45, 0x73, 0x63, 0x72, 0x69, 0x62,
	0x69, 0x72, 0x44, 0x61, 0x74, 0x61, 0x12, 0x0e, 0x2e, 0x65, 0x73, 0x63, 0x72, 0x69, 0x62, 0x69,
	0x72, 0x2e, 0x44, 0x61, 0x74, 0x61, 0x1a, 0x0e, 0x2e, 0x65, 0x73, 0x63, 0x72, 0x69, 0x62, 0x69,
	0x72, 0x2e, 0x44, 0x61, 0x74, 0x61, 0x22, 0x00, 0x28, 0x01, 0x42, 0x0c, 0x5a, 0x0a, 0x2e, 0x3b,
	0x65, 0x73, 0x63, 0x72, 0x69, 0x62, 0x69, 0x72, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_escribir_proto_rawDescOnce sync.Once
	file_escribir_proto_rawDescData = file_escribir_proto_rawDesc
)

func file_escribir_proto_rawDescGZIP() []byte {
	file_escribir_proto_rawDescOnce.Do(func() {
		file_escribir_proto_rawDescData = protoimpl.X.CompressGZIP(file_escribir_proto_rawDescData)
	})
	return file_escribir_proto_rawDescData
}

var file_escribir_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_escribir_proto_goTypes = []interface{}{
	(*Data)(nil), // 0: escribir.Data
}
var file_escribir_proto_depIdxs = []int32{
	0, // 0: escribir.EscribirService.EscribirData:input_type -> escribir.Data
	0, // 1: escribir.EscribirService.EscribirData:output_type -> escribir.Data
	1, // [1:2] is the sub-list for method output_type
	0, // [0:1] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_escribir_proto_init() }
func file_escribir_proto_init() {
	if File_escribir_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_escribir_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Data); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_escribir_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_escribir_proto_goTypes,
		DependencyIndexes: file_escribir_proto_depIdxs,
		MessageInfos:      file_escribir_proto_msgTypes,
	}.Build()
	File_escribir_proto = out.File
	file_escribir_proto_rawDesc = nil
	file_escribir_proto_goTypes = nil
	file_escribir_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// EscribirServiceClient is the client API for EscribirService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type EscribirServiceClient interface {
	EscribirData(ctx context.Context, opts ...grpc.CallOption) (EscribirService_EscribirDataClient, error)
}

type escribirServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewEscribirServiceClient(cc grpc.ClientConnInterface) EscribirServiceClient {
	return &escribirServiceClient{cc}
}

func (c *escribirServiceClient) EscribirData(ctx context.Context, opts ...grpc.CallOption) (EscribirService_EscribirDataClient, error) {
	stream, err := c.cc.NewStream(ctx, &_EscribirService_serviceDesc.Streams[0], "/escribir.EscribirService/EscribirData", opts...)
	if err != nil {
		return nil, err
	}
	x := &escribirServiceEscribirDataClient{stream}
	return x, nil
}

type EscribirService_EscribirDataClient interface {
	Send(*Data) error
	CloseAndRecv() (*Data, error)
	grpc.ClientStream
}

type escribirServiceEscribirDataClient struct {
	grpc.ClientStream
}

func (x *escribirServiceEscribirDataClient) Send(m *Data) error {
	return x.ClientStream.SendMsg(m)
}

func (x *escribirServiceEscribirDataClient) CloseAndRecv() (*Data, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(Data)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// EscribirServiceServer is the server API for EscribirService service.
type EscribirServiceServer interface {
	EscribirData(EscribirService_EscribirDataServer) error
}

// UnimplementedEscribirServiceServer can be embedded to have forward compatible implementations.
type UnimplementedEscribirServiceServer struct {
}

func (*UnimplementedEscribirServiceServer) EscribirData(EscribirService_EscribirDataServer) error {
	return status.Errorf(codes.Unimplemented, "method EscribirData not implemented")
}

func RegisterEscribirServiceServer(s *grpc.Server, srv EscribirServiceServer) {
	s.RegisterService(&_EscribirService_serviceDesc, srv)
}

func _EscribirService_EscribirData_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(EscribirServiceServer).EscribirData(&escribirServiceEscribirDataServer{stream})
}

type EscribirService_EscribirDataServer interface {
	SendAndClose(*Data) error
	Recv() (*Data, error)
	grpc.ServerStream
}

type escribirServiceEscribirDataServer struct {
	grpc.ServerStream
}

func (x *escribirServiceEscribirDataServer) SendAndClose(m *Data) error {
	return x.ServerStream.SendMsg(m)
}

func (x *escribirServiceEscribirDataServer) Recv() (*Data, error) {
	m := new(Data)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

var _EscribirService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "escribir.EscribirService",
	HandlerType: (*EscribirServiceServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "EscribirData",
			Handler:       _EscribirService_EscribirData_Handler,
			ClientStreams: true,
		},
	},
	Metadata: "escribir.proto",
}
