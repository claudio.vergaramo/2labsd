package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/claudio.vergaramo/2labsd/carga"
	"gitlab.com/claudio.vergaramo/2labsd/propuestaCent"
	"google.golang.org/grpc"
)

type Download struct {
	nombrePieza string
	destino     string
}

func random(max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max)
}

func RescateDeTitulo(granlista []string) []string {
	primeraLinea := strings.Split(granlista[0], " ")
	numero, _ := strconv.Atoi(primeraLinea[1])

	var titulos []string
	titulos = append(titulos, primeraLinea[0])

	for k := 0; k < len(granlista); k = k + numero + 1 {

		numero, _ = strconv.Atoi(primeraLinea[1])
		if k != 0 {
			primeraLinea = strings.Split(granlista[k], " ")
			titulos = append(titulos, primeraLinea[0])
		}

	}

	return titulos
}

func Pop(s []carga.Chunk, index int) []carga.Chunk {
	return append(s[:index], s[index+1:]...)
}

func RescateDeDireccion(granlista []string, nombreLibro string) []string {
	primeraLinea := strings.Split(granlista[0], " ")
	numero, _ := strconv.Atoi(primeraLinea[1])

	var partes []string

	for k := 0; k < len(granlista); k = k + numero + 1 {

		primeraLinea = strings.Split(granlista[k], " ")
		numero, _ = strconv.Atoi(primeraLinea[1])

		if nombreLibro == primeraLinea[0] {
			for x := k; x < k+numero; x++ {
				partes = append(partes, granlista[x+1])
			}
			break
		}
	}

	return partes
}

func main() {
	var i int
	var listalibros []string

	root := "./Libros"

	fmt.Println("¿Que desea hacer? : \n1.-Cargar libros\n2.-Descargar libros")
	fmt.Println("Digite el número de la opción: ")
	fmt.Scan(&i)

	if i == 1 {
		puertos := []string{":9002", ":9003", ":9004"} //en la maquina debe ir "dist12X", donde X es el numero de la maquina virtual

		//log.Printf("PUERTO ALEATORIO: %s", puertos[rand.Intn(2)])

		puertoSel := random(len(puertos))

		conn, errconn := grpc.Dial(puertos[puertoSel], grpc.WithInsecure()) //Conexion datanode

		if errconn == nil {
			log.Printf("conexion hecha correctamente")

		} else {
			log.Fatalf("Nodo no activo (DataNode %s), por favor, intente nuevamente. (ERROR: %s)", puertoSel, errconn)

		}

		defer conn.Close()

		fmt.Println("¿Que libro quieres cargar? :")
		err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
			listalibros = append(listalibros, info.Name())
			return nil
		})
		if err != nil {
			panic(err)
		}

		listalibros = listalibros[1:]

		var algoritmo int32 = 1
		var n int

		for _, file := range listalibros {

			fmt.Printf("%d.-%s\n", i, file)
			i = i + 1
		}
		fmt.Println("")

		fmt.Println("Digite el número de la opción: ")

		fmt.Scan(&n)

		fmt.Println("¿Que desea hacer? : \n1.-Exclusion mutua centralizadp\n2.-Exclusion mutua distribuida")
		fmt.Println("Digite el número de la opción: ")
		fmt.Scan(&algoritmo)

		libroSel := "./Libros/" + listalibros[n-1]

		up := carga.NewCargaServiceClient(conn)

		response, err := up.UploadData(context.Background())

		if err != nil {
			log.Printf("ERROR en cliente.go: %s", err)
		}

		//Romper libro >:c

		fileToBeChunked := libroSel

		file, err := os.Open(fileToBeChunked)

		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		defer file.Close()

		fileInfo, _ := file.Stat()

		var fileSize int64 = fileInfo.Size()

		const fileChunk = 256000 //0.256 * (1 << 20) 1 MB, change this to your requirement

		// calculate total number of parts the file will be chunked into

		totalPartsNum := uint64(math.Ceil(float64(fileSize) / float64(fileChunk)))

		for x := uint64(0); x < totalPartsNum; x++ {

			partSize := int(math.Min(fileChunk, float64(fileSize-int64(x*fileChunk))))
			partBuffer := make([]byte, partSize)

			file.Read(partBuffer)

			fileName := listalibros[n-1] + "_" + strconv.FormatUint(x, 10)

			message := carga.Data{
				Body:           fileName,
				ChunkData:      partBuffer,
				Tipo:           algoritmo,
				NombreLibro:    listalibros[n-1],
				CantidadChunks: totalPartsNum,
			}

			time.Sleep(1 * time.Second)

			err2 := response.Send(&message)

			if err2 != nil {
				log.Fatalf("Error cuando llama a SEND: %s", err2)
			}

		}

		_, err4 := response.CloseAndRecv()

		if err4 != nil {
			log.Printf("Libro cargado con éxito! :D")
		}

	} else {
		pedir := propuestaCent.Pedir{
			Pedido: "porfis",
		}

		conn, err := grpc.Dial(":9001", grpc.WithInsecure()) //Conexion NameNode

		if err != nil {
			log.Fatalf("Error con el puerto 9001: %s", err)
		}

		up := propuestaCent.NewRegistroServiceClient(conn)

		granString, errString := up.PedirLibros(context.Background(), &pedir)

		if errString != nil {
			log.Fatalf("ERROR PARA LEER : %s", errString)
		}

		log.Printf("PEDIDO: %s", granString.Pedido)

		granlistagran := strings.Split(string(granString.Pedido), "\n")

		listaString := RescateDeTitulo(granlistagran)
		var x int

		for numero, titulo := range listaString {
			fmt.Printf("%d.-%s\n", numero+1, titulo)

			//x = x + 1
		}
		fmt.Println("\nDigite el número de la opción: ")

		fmt.Scan(&x)

		libroSelected := listaString[x-1]

		log.Printf("Eligio la opción: %s", libroSelected)

		partes := RescateDeDireccion(granlistagran, libroSelected)
		log.Printf("PARTES: %s", partes)

		var listaChunkVueltos []carga.Chunk

		for y := 0; y < len(partes); y++ {
			piece := strings.Split(partes[y], " ")

			ChunkDeVuelta := carga.Descarga{
				Pieza:  piece[0],
				Puerto: piece[1],
			}

			numPuerto, _ := strconv.Atoi(ChunkDeVuelta.Puerto[len(ChunkDeVuelta.Puerto)-4:])
			numPuerto = numPuerto + 6 //strconv.Atoi(lineachunk.Puerto[len(lineachunk.Puerto)-12:])
			puertoNuevo := ChunkDeVuelta.Puerto[:len(ChunkDeVuelta.Puerto)-4] + strconv.Itoa(numPuerto)

			log.Printf("%s", puertoNuevo)

			conn, err := grpc.Dial(":9009", grpc.WithInsecure()) //puertoNuevo

			if err != nil {
				log.Printf("Error al conectar con nodo %s", puertoNuevo)
			}

			Downl := carga.NewCargaServiceClient(conn)

			respuestaChunk, errorD := Downl.DownloadData(context.Background(), &ChunkDeVuelta)

			if errorD != nil {
				log.Printf("ERROR al recuperar libro solicitado: %s", errorD)
			}

			listaChunkVueltos = append(listaChunkVueltos, *respuestaChunk)
			//ENVIAR NOMBRES PARA TRAER CHUNKS
		}

		//ESCRIBIR CHUNKS A LIBRO

		_, err = os.Create(libroSelected)

		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		var nombres []string
		var listaOrden []carga.Chunk

		for p := 0; p < len(listaChunkVueltos); p++ {
			nombres = append(nombres, listaChunkVueltos[p].NombreParte)
		}

		sort.Strings(nombres)

		for o := 0; o < len(nombres); o = o + 1 {
			for des := 0; des < len(listaChunkVueltos); des++ {
				if nombres[o] == listaChunkVueltos[des].NombreParte {
					listaOrden = append(listaOrden, listaChunkVueltos[des])
					listaChunkVueltos = Pop(listaChunkVueltos, des)
					break
				}
			}
		}

		//file, err := os.OpenFile(libroSelected, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		var elementoEscribir byte
		var listaEscribir []byte

		for i := 0; i < len(listaOrden); i++ {
			for k := 0; k < len(listaOrden[i].Chu); k++ {
				elementoEscribir = listaOrden[i].Chu[k]
				listaEscribir = append(listaEscribir, elementoEscribir)
			}
		}
		ioutil.WriteFile(libroSelected, listaEscribir, os.ModeAppend)
		log.Printf("LIBRO DESCARGADO CON ÉXITO: %s", libroSelected)
	}

}
