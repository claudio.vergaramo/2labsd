package carga

import (
	"bufio"
	context "context"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/claudio.vergaramo/2labsd/escribir"
	"gitlab.com/claudio.vergaramo/2labsd/propuestaCent"
	"google.golang.org/grpc"
)

type Server struct {
}

type Datos struct {
	body       string
	chunk      []byte
	tipo       int32
	nombrel    string
	cantchunks uint64
}

type Chun struct {
	chunkcito byte
}

func EnviarANodo(subqueue []Datos, numpuerto int) error {
	var tiempo time.Duration = 2000000000
	puertos := [3]string{":9005", ":9006", ":9007"} //DN1 = 9005; DN2 = 9006; DN3 = 9007;
	conexionDN, errDN := grpc.Dial(puertos[numpuerto], grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(tiempo))

	if errDN != nil {
		log.Fatalf("Se casho el sistema (carga.go): %s", errDN)
	}

	defer conexionDN.Close()

	up := escribir.NewEscribirServiceClient(conexionDN)
	response, err := up.EscribirData(context.Background())
	if err != nil {
		log.Printf("ERROR: %s", err)
	}

	for i := 0; i < len(subqueue); i++ {
		message := escribir.Data{
			Body:           subqueue[i].body,
			ChunkData:      subqueue[i].chunk,
			Tipo:           subqueue[i].tipo,
			NombreLibro:    subqueue[i].nombrel,
			CantidadChunks: subqueue[i].cantchunks,
		}

		err2 := response.Send(&message)

		if err2 != nil {
			log.Printf("ERROR en UploadChunks: %s", err2)

		}
	}
	_, err4 := response.CloseAndRecv()

	if err4 != nil && err4 != io.EOF {
		log.Printf("error4 en carga.go: %v", err4)
	}

	return nil
}

func TraerChunks(chunkCompleto Descarga) Chunk {
	var chunkie Chunk
	currentChunkFileName := chunkCompleto.Pieza

	newFileChunk, err := os.Open(currentChunkFileName)

	if err != nil {
		log.Printf("%s", err)

	}
	defer newFileChunk.Close()

	chunkInfo, err := newFileChunk.Stat()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	var chunkSize int64 = chunkInfo.Size()
	chunkBufferBytes := make([]byte, chunkSize)
	reader := bufio.NewReader(newFileChunk)
	_, err = reader.Read(chunkBufferBytes)

	chunkie.Chu = chunkBufferBytes
	chunkie.NombreParte = chunkCompleto.Pieza
	//chunkie.Chu = fileInfo
	//IR A BUSCAR NODOS AL DATANODE

	return chunkie

}

func EscribirDistribuidamente(ndistrodata1 string, ndistrodata2 string, ndistrodata3 string, nLibro string, numChunks uint64, up propuestaCent.RegistroServiceClient) {
	nuevaPropuesta := ArmarPropuesta(ndistrodata1, ndistrodata2, ndistrodata3, nLibro, numChunks)
	log.Printf("PROPUESTA DISTRODATA1: %s", nuevaPropuesta.DistroData1)

	mensaje, err := up.EscribirDistro(context.Background(), &nuevaPropuesta)

	if err != nil {
		log.Fatalf("ERROR en Escribir LOG distribuido: %v", err)
	}

	log.Printf("%s", mensaje.NombreLibro)
}

func ReOrdenarChunks(lista string, queueCent []Datos) []Datos {
	var listaReturn1 []Datos
	listaData := strings.Split(lista, ";")

	for i := 0; i < len(queueCent); i++ {
		for k := 0; k < len(listaData); k++ {
			if listaData[k] == queueCent[i].body {

				listaReturn1 = append(listaReturn1, queueCent[i])
				break
			}
		}
	}

	return listaReturn1
}

func ArmarPropuesta(ndistrodata1 string, ndistrodata2 string, ndistrodata3 string, nLibro string, numChunks uint64) propuestaCent.Message {
	propuestaNueva := propuestaCent.Message{
		DistroData1:    ndistrodata1,
		DistroData2:    ndistrodata2,
		DistroData3:    ndistrodata3,
		NombreLibro:    nLibro,
		CantidadChunks: numChunks,
	}

	return propuestaNueva
}

func (s *Server) UploadData(src CargaService_UploadDataServer) error {
	var queueCent []Datos
	conn, err1 := grpc.Dial(":9001", grpc.WithInsecure())

	if err1 != nil {
		log.Fatalf("Se cayo en la carga del UploadData (carga.go)")
	}

	defer conn.Close()

	for {
		rr, err := src.Recv()

		if err == io.EOF {
			break
		}

		if rr.Tipo == 1 {
			log.Printf("Chunk recibido por el server centralizado")
		} else {
			log.Printf("Chunk recibido por el server distribuido")
		}

		var datos = Datos{
			body:       rr.Body,
			chunk:      rr.ChunkData,
			tipo:       rr.Tipo,
			nombrel:    rr.NombreLibro,
			cantchunks: rr.CantidadChunks,
		}
		queueCent = append(queueCent, datos)

	}

	//Armado de propuesta

	var subqueue1 []Datos
	var subqueue2 []Datos
	var subqueue3 []Datos

	cantChunks := len(queueCent)
	module := cantChunks % 3

	//directamente multiplos de 3

	for k := 0; k < (cantChunks - module); k = k + 3 {
		for i := 0; i < 3; i++ {
			if i == 0 {
				subqueue1 = append(subqueue1, queueCent[k])
			}
			if i == 1 {
				subqueue2 = append(subqueue2, queueCent[k+1])
			}
			if i == 2 {
				subqueue3 = append(subqueue3, queueCent[k+2])
			}
		}

	}
	if module != 0 {
		if module == 1 {
			subqueue1 = append(subqueue1, queueCent[cantChunks-1])
		}

		if module == 2 {
			subqueue1 = append(subqueue1, queueCent[cantChunks-2])
			subqueue2 = append(subqueue2, queueCent[cantChunks-1])
		}
	}

	//Excedentes de 3

	var registroNode1 []string
	var registroNode3 []string
	var registroNode2 []string
	var nombre string

	for x := 0; x < len(subqueue1); x++ {
		nombre = subqueue1[x].body
		registroNode1 = append(registroNode1, nombre)

	}

	for x := 0; x < len(subqueue2); x++ {
		nombre = subqueue2[x].body
		registroNode2 = append(registroNode2, nombre)

	}

	for x := 0; x < len(subqueue3); x++ {
		nombre = subqueue3[x].body
		registroNode3 = append(registroNode3, nombre)
	}

	var registroCompleto1 string = strings.Join(registroNode1, ";")
	var registroCompleto2 string = strings.Join(registroNode2, ";")
	var registroCompleto3 string = strings.Join(registroNode3, ";")

	log.Printf("%s", queueCent[0].nombrel)

	propuesta := propuestaCent.Message{
		DistroData1:    registroCompleto1,
		DistroData2:    registroCompleto2,
		DistroData3:    registroCompleto3,
		NombreLibro:    queueCent[0].nombrel,
		CantidadChunks: queueCent[0].cantchunks,
	}

	up := propuestaCent.NewRegistroServiceClient(conn)

	///CENTRALIZADO///--------------------------------------------------------
	if queueCent[0].tipo == 1 {
		listaData, err := up.EnviarDistro(context.Background(), &propuesta) //CENTRALIZADO

		if err != nil {
			log.Printf("Error en recepcion del mensaje desde nameNode (carga.go): %s", listaData.NombreLibro)
		}

		//Genera Propuesta 2 nodos activos
		if listaData.Aprobado != "OK" {
			//REORDEN
			if listaData.DistroData1 == "" && listaData.DistroData2 != "" && listaData.DistroData3 != "" { //Nodo 2 y 3 funcionan
				subqueue2 = ReOrdenarChunks(listaData.DistroData2, queueCent)
				subqueue3 = ReOrdenarChunks(listaData.DistroData3, queueCent)

				EnviarANodo(subqueue2, 1)
				EnviarANodo(subqueue3, 2)
			} else if listaData.DistroData1 != "" && listaData.DistroData2 == "" && listaData.DistroData3 != "" { //Nodo 1 y 3 funcionan
				subqueue1 = ReOrdenarChunks(listaData.DistroData1, queueCent)
				subqueue3 = ReOrdenarChunks(listaData.DistroData3, queueCent)

				EnviarANodo(subqueue1, 0)
				EnviarANodo(subqueue3, 2)
			} else if listaData.DistroData1 != "" && listaData.DistroData2 != "" && listaData.DistroData3 == "" { //Nodo 1 y 2 funcionan
				subqueue1 = ReOrdenarChunks(listaData.DistroData1, queueCent)
				subqueue2 = ReOrdenarChunks(listaData.DistroData2, queueCent)
				EnviarANodo(subqueue1, 0)
				EnviarANodo(subqueue2, 1)
			} else if listaData.DistroData1 != "" && listaData.DistroData2 == "" && listaData.DistroData3 == "" { //Nodo 1 func
				subqueue1 = queueCent

				EnviarANodo(subqueue1, 0)
			} else if listaData.DistroData1 == "" && listaData.DistroData2 != "" && listaData.DistroData3 == "" { //Nodo 2 func
				subqueue2 = queueCent

				EnviarANodo(subqueue2, 1)
			} else if listaData.DistroData1 == "" && listaData.DistroData2 == "" && listaData.DistroData3 != "" { //Nodo 3 func
				subqueue3 = queueCent

				EnviarANodo(subqueue3, 2)
			} else if listaData.DistroData1 == "" && listaData.DistroData2 == "" && listaData.DistroData3 == "" { //NOFUNCA
				log.Fatalf("Todos los server caidos!")
			}
		} else {
			EnviarANodo(subqueue1, 0)
			EnviarANodo(subqueue2, 1)
			EnviarANodo(subqueue3, 2)
		}
	} else { //Distribuido-----------------
		var tiempo time.Duration = 3000000000
		var EstadoDN1 int
		var EstadoDN2 int
		var EstadoDN3 int
		var nDistroData1 string
		var nDistroData2 string
		var nDistroData3 string

		conexionDN1, errDN1 := grpc.Dial(":9005", grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(tiempo))
		conexionDN2, errDN2 := grpc.Dial(":9006", grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(tiempo))
		conexionDN3, errDN3 := grpc.Dial(":9007", grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(tiempo))

		if errDN1 != nil {
			log.Printf("DATANODE1 Se cayo (9005)")
			EstadoDN1 = 0
		} else {
			EstadoDN1 = 1
			log.Printf("DataNode1 OK")
			defer conexionDN1.Close()
		}

		if errDN2 != nil {
			log.Printf("DATANODE2 Se cayo (9006)")
			EstadoDN2 = 0
		} else {
			EstadoDN2 = 1
			log.Printf("DataNode2 OK")
			defer conexionDN2.Close()
		}

		if errDN3 != nil {
			log.Printf("DATANODE3 Se cayo (9007)")
			EstadoDN3 = 0
		} else {
			EstadoDN3 = 1
			log.Printf("DataNode3 OK")
			defer conexionDN3.Close()
		}

		//REDISTRIBUCION --

		listaDD1 := strings.Split(propuesta.DistroData1, ";")
		listaDD2 := strings.Split(propuesta.DistroData2, ";")
		listaDD3 := strings.Split(propuesta.DistroData3, ";")

		if EstadoDN1 == 1 && EstadoDN2 == 1 && EstadoDN3 == 1 {
			EscribirDistribuidamente(propuesta.DistroData1, propuesta.DistroData2, propuesta.DistroData3, queueCent[0].nombrel, queueCent[0].cantchunks, up)
			EnviarANodo(subqueue1, 0)
			EnviarANodo(subqueue2, 1)
			EnviarANodo(subqueue3, 2)

		} else if EstadoDN1 == 1 && EstadoDN2 == 0 && EstadoDN3 == 1 {
			nDistroData1, nDistroData3 = propuestaCent.RePropuesta1Caido(listaDD1, listaDD2, listaDD3)
			subqueue1 = ReOrdenarChunks(nDistroData1, queueCent)
			subqueue3 = ReOrdenarChunks(nDistroData3, queueCent)

			EscribirDistribuidamente(nDistroData1, nDistroData2, nDistroData3, queueCent[0].nombrel, queueCent[0].cantchunks, up)
			EnviarANodo(subqueue1, 0)
			EnviarANodo(subqueue3, 2)

		} else if EstadoDN1 == 1 && EstadoDN2 == 1 && EstadoDN3 == 0 {
			nDistroData1, nDistroData2 = propuestaCent.RePropuesta1Caido(listaDD1, listaDD2, listaDD3)
			subqueue1 = ReOrdenarChunks(nDistroData1, queueCent)
			subqueue2 = ReOrdenarChunks(nDistroData2, queueCent)
			EscribirDistribuidamente(nDistroData1, nDistroData2, nDistroData3, queueCent[0].nombrel, queueCent[0].cantchunks, up)
			EnviarANodo(subqueue1, 0)
			EnviarANodo(subqueue2, 1)

		} else if EstadoDN1 == 0 && EstadoDN2 == 1 && EstadoDN3 == 1 {
			nDistroData2, nDistroData3 = propuestaCent.RePropuesta1Caido(listaDD1, listaDD2, listaDD3)
			subqueue2 = ReOrdenarChunks(nDistroData2, queueCent)
			subqueue3 = ReOrdenarChunks(nDistroData3, queueCent)
			EscribirDistribuidamente(nDistroData1, nDistroData2, nDistroData3, queueCent[0].nombrel, queueCent[0].cantchunks, up)
			EnviarANodo(subqueue2, 1)
			EnviarANodo(subqueue3, 2)

		} else if EstadoDN1 == 0 && EstadoDN2 == 0 && EstadoDN3 == 1 {
			nDistroData3 = propuestaCent.RePropuesta2Caidos(listaDD1, listaDD2, listaDD3)
			subqueue3 = ReOrdenarChunks(nDistroData3, queueCent)
			EscribirDistribuidamente(nDistroData1, nDistroData2, nDistroData3, queueCent[0].nombrel, queueCent[0].cantchunks, up)
			EnviarANodo(subqueue3, 2)

		} else if EstadoDN1 == 1 && EstadoDN2 == 0 && EstadoDN3 == 0 {
			nDistroData1 = propuestaCent.RePropuesta2Caidos(listaDD1, listaDD2, listaDD3)
			subqueue1 = ReOrdenarChunks(nDistroData1, queueCent)
			EscribirDistribuidamente(nDistroData1, nDistroData2, nDistroData3, queueCent[0].nombrel, queueCent[0].cantchunks, up)
			EnviarANodo(subqueue1, 0)

		} else if EstadoDN1 == 0 && EstadoDN2 == 1 && EstadoDN3 == 0 {
			nDistroData2 = propuestaCent.RePropuesta2Caidos(listaDD1, listaDD2, listaDD3)
			subqueue2 = ReOrdenarChunks(nDistroData2, queueCent)
			EnviarANodo(subqueue2, 1)

		} else if EstadoDN1 == 0 && EstadoDN2 == 0 && EstadoDN3 == 0 {
			nDistroData1 = "ERROR EN DD1"
			nDistroData2 = "ERROR EN DD2"
			nDistroData3 = "ERROR EN DD3"
			log.Fatalf("ERROR EN TODOS LOS NODOS")
		}

	}

	return nil
}

func (s *Server) DownloadData(ctx context.Context, in *Descarga) (*Chunk, error) {
	var chunkADevolver Chunk
	chunkADevolver = TraerChunks(*in)
	log.Printf("HOLA: %s", in.Pieza)

	return &chunkADevolver, nil
}
