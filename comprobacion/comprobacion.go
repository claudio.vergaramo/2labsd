package comprobacion

import (
	"log"

	"golang.org/x/net/context"
)

type Server struct {
}

func (s *Server) Comprobar(ctx context.Context, message *Message) (*Message, *Message) {
	log.Printf("Message received: %s", message.Body)
	return &Message{Body: "Vivo!"}, &Message{Body: "SE MURIO!"}
}
