package main

import (
	"log"
	"net"

	"gitlab.com/claudio.vergaramo/2labsd/carga"
	"gitlab.com/claudio.vergaramo/2labsd/escribir"
	"google.golang.org/grpc"
)

func main() {

	go func() {
		lis, err := net.Listen("tcp", ":9002") //conexion cliente -- SERVER

		if err != nil {
			log.Fatalf("Se casho el sistema: %s", err)
		}

		s := carga.Server{}

		grpcServer := grpc.NewServer()

		carga.RegisterCargaServiceServer(grpcServer, &s)

		if err := grpcServer.Serve(lis); err != nil {
			log.Fatalf("Falla del server en el puerto 9002: %v", err)

		}
	}()

	go func() {
		escuchaDN1, err := net.Listen("tcp", ":9005") //conexion para propuestas de distribución
		if err != nil {
			log.Fatalf("Se casho el sistema: %s", err)
		}

		x := escribir.Server{}

		grpcServer := grpc.NewServer()

		escribir.RegisterEscribirServiceServer(grpcServer, &x)

		if err := grpcServer.Serve(escuchaDN1); err != nil {
			log.Fatalf("Falla del server en el puerto 9005: %v", err)
		}
	}()

	lisDN1, err := net.Listen("tcp", ":9008") //conexion para rescatar Chunks

	if err != nil {
		log.Fatalf("Se casho el sistema: %s", err)
	}

	x := carga.Server{}
	grpcServer := grpc.NewServer()

	carga.RegisterCargaServiceServer(grpcServer, &x)

	if err := grpcServer.Serve(lisDN1); err != nil {
		log.Fatalf("Falla del server en el puerto 9008: %v", err)
	}

}
