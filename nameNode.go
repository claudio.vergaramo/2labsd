package main

import (
	"log"
	"net"

	"gitlab.com/claudio.vergaramo/2labsd/propuestaCent"
	"google.golang.org/grpc"
)

func main() {
	lis, err := net.Listen("tcp", ":9001")
	if err != nil {
		log.Fatalf("Error en el puerto 9001: %v", err)
	}

	s := propuestaCent.Server{}

	grpcServer := grpc.NewServer()

	propuestaCent.RegisterRegistroServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Fallo con conexion gRPC en el puerto 9001: %s", err)
	}

}
