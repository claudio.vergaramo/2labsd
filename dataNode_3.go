package main

import (
	"log"
	"net"

	"gitlab.com/claudio.vergaramo/2labsd/carga"
	"gitlab.com/claudio.vergaramo/2labsd/escribir"
	"google.golang.org/grpc"
)

func main() {
	go func() {
		lis, err := net.Listen("tcp", ":9004") //conexion cliente -- SERVER

		if err != nil {
			log.Fatalf("Se casho el sistema: %s", err)
		}

		s := carga.Server{}

		grpcServer := grpc.NewServer()

		carga.RegisterCargaServiceServer(grpcServer, &s)

		if err := grpcServer.Serve(lis); err != nil {
			log.Fatalf("Falla del server en el puerto 9004: %v", err)
		}
	}()

	go func() {
		escuchaDN3, err := net.Listen("tcp", ":9007") //conexion para propuestas de distribución

		if err != nil {
			log.Fatalf("Se casho el sistema: %s", err)
		}

		x := escribir.Server{}

		grpcServer := grpc.NewServer()

		escribir.RegisterEscribirServiceServer(grpcServer, &x)

		if err := grpcServer.Serve(escuchaDN3); err != nil {
			log.Fatalf("Falla del server en el puerto 9007: %v", err)
		}
	}()

	lisDN3, err := net.Listen("tcp", ":9010") //conexion para propuestas de distribución

	if err != nil {
		log.Fatalf("Se casho el sistema: %s", err)
	}

	x := carga.Server{}

	grpcServer := grpc.NewServer()

	carga.RegisterCargaServiceServer(grpcServer, &x)

	if err := grpcServer.Serve(lisDN3); err != nil {
		log.Fatalf("Falla del server en el puerto 9010: %v", err)
	}
}
