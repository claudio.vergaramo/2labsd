package propuestaCent

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type Server struct {
}

func RePropuesta1Caido(lista1 []string, lista2 []string, lista3 []string) (string, string) {

	var granlista []string
	var subqueue1 []string
	var subqueue2 []string

	for i := 0; i < len(lista1); i++ {
		granlista = append(granlista, lista1[i])
	}
	for i := 0; i < len(lista2); i++ {
		granlista = append(granlista, lista2[i])
	}
	for i := 0; i < len(lista3); i++ {
		granlista = append(granlista, lista3[i])
	}

	module := (len(granlista)) % 2

	//directamente multiplos de 2

	for k := 0; k < (len(granlista) - module); k = k + 2 {
		for i := 0; i < 2; i++ {
			if i == 0 {
				subqueue1 = append(subqueue1, granlista[k])
			}
			if i == 1 {
				subqueue2 = append(subqueue2, granlista[k+1])
			}
		}
	}
	if module == 1 {
		subqueue1 = append(subqueue1, granlista[len(granlista)-1])
	}

	var cola1 string = strings.Join(subqueue1, ";")
	var cola2 string = strings.Join(subqueue2, ";")

	return cola1, cola2
}

func RePropuesta2Caidos(lista1 []string, lista2 []string, lista3 []string) string {
	var granlista []string

	for i := 0; i < len(lista1); i++ {
		granlista = append(granlista, lista1[i])
	}
	for i := 0; i < len(lista2); i++ {
		granlista = append(granlista, lista2[i])
	}
	for i := 0; i < len(lista3); i++ {
		granlista = append(granlista, lista3[i])
	}

	var cola string = strings.Join(granlista, ";")

	return cola
}

func EscribirLog(nombre string, cantidadchunks uint64, data1 string, data2 string, data3 string) error {
	var nombreParte1 string
	var nombreParte2 string
	var nombreParte3 string
	var datoslista1 []string
	var datoslista2 []string
	var datoslista3 []string

	datoslista1 = strings.Split(data1, ";")
	datoslista2 = strings.Split(data2, ";")
	datoslista3 = strings.Split(data3, ";")

	strCantChunks := strconv.FormatUint(cantidadchunks, 10)

	nombreyCantidad := (nombre + " " + strCantChunks + "\n")

	f, err := os.OpenFile("LOG.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		fmt.Println(err)
	}

	_, e := f.WriteString(nombreyCantidad)

	defer f.Close()
	f, err = os.OpenFile("LOG.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	for x := 0; x < len(datoslista1); x++ {
		nombreParte1 = datoslista1[x]
		if nombreParte1 == "" {
			break
		}
		f.WriteString(nombreParte1 + " dist122:9002\n")
	}

	defer f.Close()
	f, err = os.OpenFile("LOG.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	for x := 0; x < len(datoslista2); x++ {
		nombreParte2 = datoslista2[x]
		if nombreParte2 == "" {
			break
		}
		f.WriteString(nombreParte2 + " dist123:9003\n")
	}

	defer f.Close()
	f, err = os.OpenFile("LOG.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	for x := 0; x < len(datoslista3); x++ {
		nombreParte3 = datoslista3[x]
		if nombreParte3 == "" {
			break
		}
		f.WriteString(nombreParte3 + " dist124:9004\n")

	}

	if e != nil {
		panic(e)
	}

	defer f.Close()
	return nil
}

func (s *Server) EnviarDistro(ctx context.Context, message *Message) (*Message, error) {
	//log.Printf("\nReceived DataNode1: %s\nReceived DataNode2: %s\nReceived DataNode3: %s", message.DistroData1, message.DistroData2, message.DistroData3)

	var tiempo time.Duration = 3000000000

	var nDistroData1 string
	var nDistroData2 string
	var nDistroData3 string
	var EstadoDN1 int
	var EstadoDN2 int
	var EstadoDN3 int
	var Recepcion string

	conexionDN1, errDN1 := grpc.Dial(":9002", grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(tiempo))
	conexionDN2, errDN2 := grpc.Dial(":9003", grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(tiempo))
	conexionDN3, errDN3 := grpc.Dial(":9004", grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(tiempo))

	if errDN1 != nil {
		log.Printf("Se cayo 9002")
		EstadoDN1 = 0
	} else {
		EstadoDN1 = 1
		defer conexionDN1.Close()
	}

	if errDN2 != nil {
		log.Printf("Se cayo 9003")
		EstadoDN2 = 0
	} else {
		EstadoDN2 = 1
		defer conexionDN2.Close()
	}

	if errDN3 != nil {
		log.Printf("Se cayo 9004")
		EstadoDN3 = 0
	} else {
		EstadoDN3 = 1
		defer conexionDN3.Close()
	}

	listaDD1 := strings.Split(message.DistroData1, ";")
	listaDD2 := strings.Split(message.DistroData2, ";")
	listaDD3 := strings.Split(message.DistroData3, ";")

	if EstadoDN1 == 1 && EstadoDN2 == 1 && EstadoDN3 == 1 {
		nDistroData1 = message.DistroData1
		nDistroData2 = message.DistroData2
		nDistroData3 = message.DistroData3
		Recepcion = "OK"

	} else if EstadoDN1 == 1 && EstadoDN2 == 0 && EstadoDN3 == 1 {
		nDistroData1, nDistroData3 = RePropuesta1Caido(listaDD1, listaDD2, listaDD3)
		Recepcion = "Repropuesta"

	} else if EstadoDN1 == 1 && EstadoDN2 == 1 && EstadoDN3 == 0 {
		nDistroData1, nDistroData2 = RePropuesta1Caido(listaDD1, listaDD2, listaDD3)
		Recepcion = "Repropuesta"

	} else if EstadoDN1 == 0 && EstadoDN2 == 1 && EstadoDN3 == 1 {
		nDistroData2, nDistroData3 = RePropuesta1Caido(listaDD1, listaDD2, listaDD3)
		Recepcion = "Repropuesta"

	} else if EstadoDN1 == 0 && EstadoDN2 == 0 && EstadoDN3 == 1 {
		nDistroData3 = RePropuesta2Caidos(listaDD1, listaDD2, listaDD3)
		Recepcion = "Repropuesta"

	} else if EstadoDN1 == 1 && EstadoDN2 == 0 && EstadoDN3 == 0 {
		nDistroData1 = RePropuesta2Caidos(listaDD1, listaDD2, listaDD3)
		Recepcion = "Repropuesta"

	} else if EstadoDN1 == 0 && EstadoDN2 == 1 && EstadoDN3 == 0 {
		nDistroData2 = RePropuesta2Caidos(listaDD1, listaDD2, listaDD3)
		Recepcion = "Repropuesta"

	} else if EstadoDN1 == 0 && EstadoDN2 == 0 && EstadoDN3 == 0 {
		nDistroData3 = RePropuesta2Caidos(listaDD1, listaDD2, listaDD3)
		Recepcion = "Repropuesta"
	} else {
		nDistroData1 = "ERROR EN DD1"
		nDistroData2 = "ERROR EN DD2"
		nDistroData3 = "ERROR EN DD3"
		Recepcion = "Repropuesta"
	}

	EscribirLog(message.NombreLibro, message.CantidadChunks, nDistroData1, nDistroData2, nDistroData3)

	return &Message{DistroData1: nDistroData1, DistroData2: nDistroData2, DistroData3: nDistroData3, NombreLibro: message.NombreLibro, CantidadChunks: message.CantidadChunks, Aprobado: Recepcion}, nil
}

func (s *Server) EscribirDistro(ctx context.Context, message *Message) (*Message, error) { //Escribir Log de tipo distribuido
	EscribirLog(message.NombreLibro, message.CantidadChunks, message.DistroData1, message.DistroData2, message.DistroData3)
	message.NombreLibro = "LOG escrito correctamente"
	return message, nil
}

func (s *Server) PedirLibros(ctx context.Context, message *Pedir) (*Pedir, error) {
	//var src RegistroService_PedirLibrosClient

	data, errRead := ioutil.ReadFile("LOG.txt")

	if errRead != nil {
		fmt.Println("LOG.txt, no encontrado o con errores :", errRead)
	}
	fmt.Println("Contents of file:", string(data))

	pedido := strings.Split(string(data), "\r\n")
	pedidoNU := strings.Join(pedido, ";")

	mensaje := Pedir{
		Pedido: pedidoNU,
	}

	return &mensaje, nil
}
